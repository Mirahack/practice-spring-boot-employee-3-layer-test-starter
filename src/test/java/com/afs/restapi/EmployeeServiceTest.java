package com.afs.restapi;

import com.afs.restapi.exception.AgeErrorException;
import com.afs.restapi.exception.AgeNotMatchSalaryException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
class EmployeeServiceTest {

    private final EmployeeRepository employeeRepository = mock(EmployeeRepository.class);

    private final EmployeeService employeeService = new EmployeeService(employeeRepository);

    @BeforeEach
    void setUp() {
        employeeRepository.clearAll();
    }

    @Test
    void should_throw_age_error_exception_when_create_given_employee_age15_and_age66() {
        Employee employeeSusan = new Employee(1, "Susan", 15, "female", 200);
        Employee employeeJason = new Employee(2, "Jason", 66, "male", 200000);

        Assertions.assertThrows(AgeErrorException.class, () -> employeeService.insertEmployee(employeeSusan));
        Assertions.assertThrows(AgeErrorException.class, () -> employeeService.insertEmployee(employeeJason));

        Mockito.verify(employeeRepository, times(0)).insert(any());
    }

    @Test
    void should_throw_age_not_match_salary_error_exception_when_create_given_employee_age_30_salary_15000() {
        Employee employeeTom = new Employee(1, "Tom", 30, "male", 15000);

        Assertions.assertThrows(AgeNotMatchSalaryException.class, () -> employeeService.insertEmployee(employeeTom));

        Mockito.verify(employeeRepository, times(0)).insert(any());
    }

    @Test
    void should_employee_status_active_true_when_create_given_employee() {
        Employee employeeTom = new Employee(1, "Tom", 25, "male", 15000);
        Employee employeeToReturn = new Employee(1, "Tom", 25, "male", 15000);
        employeeToReturn.setActiveStatus(true);
        when(employeeRepository.insert(any())).thenReturn(employeeToReturn);

        Employee employeeGet = employeeService.insertEmployee(employeeTom);
//        employeeTom.setActiveStatus(false);

        assertSame(true, employeeGet.getActiveStatus());
        verify(employeeRepository).insert(argThat(employee -> {
            assertSame(true, employee.getActiveStatus());
            return true;
        }));
    }

    @Test
    void should_employee_status_active_false_when_delete_given_employee() {
        Employee employeeToReturn = new Employee(1, "Tom", 25, "male", 15000);
        employeeToReturn.setActiveStatus(true);
        when(employeeRepository.findById(1)).thenReturn(employeeToReturn);

        employeeService.deleteEmployee(employeeToReturn.getId());

        assertSame(false, employeeToReturn.getActiveStatus());
    }

    @Test
    void should_throw_specific_errors_when_update_given_onboard_employee_and_left_employee() {
        Employee employee = new Employee(1, "aa", 20, "male", 10000);
        employee.setActiveStatus(false);

        employeeService.updateEmployee(1, employee);
        verify(employeeRepository, times(0)).update(1, employee);
    }
}
