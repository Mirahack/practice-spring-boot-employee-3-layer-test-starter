package com.afs.restapi;

import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CompanyControllerTest {
    @Autowired
    MockMvc mockMvc;
    ObjectMapper mapper = new ObjectMapper();
    @Autowired
    CompanyRepository companyRepository;

    @BeforeEach
    void setUp() {
        companyRepository.clearAll();
    }

    @Test
    void should_get_all_employees_when_perform_get_given_employees() throws Exception {
        //given
        Company company = buildCompanyCa();
        companyRepository.addCompany(company);

        //when
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/employees"))
                .andReturn();

        //then
        MockHttpServletResponse response = mvcResult.getResponse();
        assertEquals(200, response.getStatus());
        List<Employee> employees = mapper.readValue(response.getContentAsString(), new TypeReference<List<Employee>>() {
        });
        assertEquals(6, employees.size());
        Employee employeeGet = employees.get(0);
        assertEquals(1, employeeGet.getId());
        assertEquals("Lily1", employeeGet.getName());
        assertEquals(20, employeeGet.getAge());
        assertEquals("Female", employeeGet.getGender());
        assertEquals(8000, employeeGet.getSalary());
    }

    private static List<Employee> buildEmployeeList() {
        return new ArrayList<>(List.of(
                new Employee(1, "Susan", 22, "Female", 10000),
                new Employee(2, "Lisi", 30, "Man", 5000),
                new Employee(3, "WangWu", 30, "Man", 6000)
        ));
    }

    private static Company buildCompanyCa() {
        return new Company(1, "Ca", buildEmployeeList());
    }

    @Test
    void should_get_all_companies_when_perform_get_given_company() throws Exception {
        //given
        companyRepository.addCompany(buildCompanyCa());

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].companyName").isString())
                .andExpect(jsonPath("$[0].companyName").value("Ca"))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[0].id").value(buildEmployeeList().get(0).getId()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[0].name").value(buildEmployeeList().get(0).getName()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[0].age").value(buildEmployeeList().get(0).getAge()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[0].gender").value(buildEmployeeList().get(0).getGender()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[0].salary").value(buildEmployeeList().get(0).getSalary()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[1].id").value(buildEmployeeList().get(1).getId()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[1].name").value(buildEmployeeList().get(1).getName()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[1].age").value(buildEmployeeList().get(1).getAge()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[1].gender").value(buildEmployeeList().get(1).getGender()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[1].salary").value(buildEmployeeList().get(1).getSalary()));
        ;
    }


    @Test
    void should_return_company_when_perform_get_by_id_given_company() throws Exception {
        //given
        companyRepository.addCompany(buildCompanyCa());

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.companyName").isString())
                .andExpect(jsonPath("$.companyName").value("Ca"))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[0].id").value(buildEmployeeList().get(0).getId()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[0].name").value(buildEmployeeList().get(0).getName()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[0].age").value(buildEmployeeList().get(0).getAge()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[0].gender").value(buildEmployeeList().get(0).getGender()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[0].salary").value(buildEmployeeList().get(0).getSalary()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[1].id").value(buildEmployeeList().get(1).getId()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[1].name").value(buildEmployeeList().get(1).getName()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[1].age").value(buildEmployeeList().get(1).getAge()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[1].gender").value(buildEmployeeList().get(1).getGender()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[1].salary").value(buildEmployeeList().get(1).getSalary()));
        ;
    }

    @Test
    void should_return_employees_list_when_perform_get_by_id_given_company() throws Exception {
        //given
        companyRepository.addCompany(buildCompanyCa());

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/1/employees"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(buildEmployeeList().get(0).getId()))
                .andExpect(jsonPath("$[0].name").isString())
                .andExpect(jsonPath("$[0].name").value(buildEmployeeList().get(0).getName()))
                .andExpect(jsonPath("$[0].age").isNumber())
                .andExpect(jsonPath("$[0].age").value(buildEmployeeList().get(0).getAge()))
                .andExpect(jsonPath("$[0].gender").isString())
                .andExpect(jsonPath("$[0].gender").value(buildEmployeeList().get(0).getGender()))
                .andExpect(jsonPath("$[0].salary").isNumber())
                .andExpect(jsonPath("$[0].salary").value(buildEmployeeList().get(0).getSalary()))
                .andExpect(jsonPath("$[1].id").isNumber())
                .andExpect(jsonPath("$[1].id").value(buildEmployeeList().get(1).getId()))
                .andExpect(jsonPath("$[1].name").isString())
                .andExpect(jsonPath("$[1].name").value(buildEmployeeList().get(1).getName()))
                .andExpect(jsonPath("$[1].age").isNumber())
                .andExpect(jsonPath("$[1].age").value(buildEmployeeList().get(1).getAge()))
                .andExpect(jsonPath("$[1].gender").isString())
                .andExpect(jsonPath("$[1].gender").value(buildEmployeeList().get(1).getGender()))
                .andExpect(jsonPath("$[1].salary").isNumber())
                .andExpect(jsonPath("$[1].salary").value(buildEmployeeList().get(1).getSalary()));
        ;
    }

    @Test
    void should_return_specific_page_companies_when_perform_get_by_page_given_companies() throws Exception {
        //given
        companyRepository.addCompany(buildCompanyCa());

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies?pageNumber=1&pageSize=2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].companyName").isString())
                .andExpect(jsonPath("$[0].companyName").value("Ca"))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[0].id").value(buildEmployeeList().get(0).getId()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[0].name").value(buildEmployeeList().get(0).getName()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[0].age").value(buildEmployeeList().get(0).getAge()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[0].gender").value(buildEmployeeList().get(0).getGender()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[0].salary").value(buildEmployeeList().get(0).getSalary()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[1].id").value(buildEmployeeList().get(1).getId()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[1].name").value(buildEmployeeList().get(1).getName()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[1].age").value(buildEmployeeList().get(1).getAge()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[1].gender").value(buildEmployeeList().get(1).getGender()))
                .andExpect(jsonPath("$[0].employees").isArray())
                .andExpect(jsonPath("$[0].employees[1].salary").value(buildEmployeeList().get(1).getSalary()));
        ;
    }

    @Test
    void should_update_when_perform_put_by_id_given_update_company() throws Exception {
        //given
        companyRepository.addCompany(buildCompanyCa());

        Company toBeUpdateCompanyA = buildCompanyCa();
        toBeUpdateCompanyA.setCompanyName("Cb");

        String susanJson = mapper.writeValueAsString(toBeUpdateCompanyA);

        //when
        mockMvc.perform(MockMvcRequestBuilders.put("/companies/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(susanJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.companyName").isString())
                .andExpect(jsonPath("$.companyName").value("Cb"))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[0].id").value(buildEmployeeList().get(0).getId()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[0].name").value(buildEmployeeList().get(0).getName()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[0].age").value(buildEmployeeList().get(0).getAge()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[0].gender").value(buildEmployeeList().get(0).getGender()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[0].salary").value(buildEmployeeList().get(0).getSalary()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[1].id").value(buildEmployeeList().get(1).getId()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[1].name").value(buildEmployeeList().get(1).getName()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[1].age").value(buildEmployeeList().get(1).getAge()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[1].gender").value(buildEmployeeList().get(1).getGender()))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees[1].salary").value(buildEmployeeList().get(1).getSalary()));

        Company company = companyRepository.getCompanies().get(0);
        assertEquals(toBeUpdateCompanyA.getCompanyName(), company.getCompanyName());
    }

    @Test
    void should_return_deleted_company_when_perform_delete_given_company_and_id() throws Exception {
        //given
        companyRepository.addCompany(buildCompanyCa());


        //when
        mockMvc.perform(MockMvcRequestBuilders.delete("/companies/{id}", 1));

        //then
        Assertions.assertEquals(0, companyRepository.getCompanies().size());
    }

}
