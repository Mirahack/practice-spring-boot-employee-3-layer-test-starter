package com.afs.restapi.service;

import com.afs.restapi.exception.AgeErrorException;
import com.afs.restapi.exception.AgeNotMatchSalaryException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> findEmployeesByGender() {
        return employeeRepository.findAll();
    }

    public Employee getEmployeeById(int id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> findEmployeesByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> findByPage(int pageNumber, int pageSize) {
        return employeeRepository.findByPage(pageNumber, pageSize);
    }

    public Employee insertEmployee(Employee employee) {
        if (employee.getAge() < 18 || employee.getAge() > 65) {
            throw new AgeErrorException();
        }
        if (employee.getAge() >= 30 && employee.getSalary() < 20000) {
            throw new AgeNotMatchSalaryException();
        }
        employee.setActiveStatus(true);
        return employeeRepository.insert(employee);
    }

    public Employee updateEmployee(int id, Employee employee) {
        if (employee.getActiveStatus()) {
            return employeeRepository.update(id, employee);
        }
        return null;
    }

    public void deleteEmployee(int id) {
//        employeeRepository.delete(id);
        employeeRepository.findById(id).setActiveStatus(false);
    }
}