O:Today, we learned about the three-tier architecture of spring boot, as well as the method of spring boot Integration testing. After that, we carried out exercises on spring boot service layer extraction and Integration testing.

R:I feel quite hard.

I:I haven't touched the content of today's Integration testing before. So I am very unfamiliar with its related APIs, such as mocks, and it is difficult for me to understand some of its principles, which has caused me to encounter many difficulties when doing homework.

D:I hope to study Integration testing related content and get familiar with related apis later, until I can write Integration testing code skillfully and quickly.